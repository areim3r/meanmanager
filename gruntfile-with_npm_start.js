module.exports = function(grunt){

grunt.loadNpmTasks('grunt-express');

grunt.initConfig({
    express: {
      default_option: {
        port: 3000,
        hostname: '*'
      }
    }
});

grunt.loadNpmTasks('grunt-express');

grunt.registerTask('default', ['express', 'express-keepalive']);
    
}